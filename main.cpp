#include <QCoreApplication>

#include <string>
#include <vector>

#include <iostream>

#include "param_tree.h"




int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);



    ParamTreeItem tree(true);
    ParamTreeItem* node;

    tree.AddItem("sfsdfsdf", "133");
    tree.AddItem("fsdfsdf", "1343323");
    tree.AddItem("sdfsdf", "13433");
    node = tree.AddNode("GKGJHGJHG");
    tree.AddItem("dfsdf", "134323");
    tree.AddItem("fsdf", "13");
    tree.AddItem("df", "13432323");

    node->AddItem("cvbcvbcvb", "999967");
    node->AddItem("cvbcvb", "997");
    node = node->AddNode("POIPOIPOIOP");
    node->AddItem("iyyuiy", "035345");
    node->AddItem("iuyuyuiyui", "76575");


    std::vector<std::string> s;

    tree.GetStrings(s, false);


    for(auto str : s)
        std::cout << str << std::endl;

    std::cout << "==================================" << std::endl;

    ParamTreeItem tree2(true);
    bool res = false;
    try{
     res = tree2.AddStrings(s.begin(), s.end());
    }catch(...){}

    if(res){

        s.clear();

        tree2.GetStrings(s);
        for(auto str : s)
            std::cout << str << std::endl;
    }else
        std::cout << "tree2 error" << std::endl;

/*  test non-string template , ok  if compilation error
    ParamTreeItem tree3(true);
    std::vector<int> iii;
    tree3.AddStrings(iii.begin(), iii.end());
    */

    /* test for not vector container, of if compile
    ParamTreeItem tree3(true);
    std::deque<std::string> sss;
    tree3.AddStrings(sss.begin(), sss.end());
    */


    return a.exec();
}
