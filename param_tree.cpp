
#include "param_tree.h"



std::string ParamTreeItem::spacer = "\t";
std::string ParamTreeItem::node_prefix = "$";




void ParamTreeItem::GetStrings(std::vector<std::string>& strings, bool add_spacer){
    if(!node) return;

    std::vector<std::string> all;
    for(auto p: items)
        if(p){
            all.push_back(p->String());
            if(p->IsNode()){
                std::vector<std::string> tmp;
                p->GetStrings(tmp, add_spacer);
                for(auto s : tmp)
                    all.push_back(add_spacer?(spacer + s):s);
                tmp.clear();
                all.push_back(node_prefix+p->String());
            }
        }
    strings = std::move(all);
}

bool ParamTreeItem::add_item(std::string str)
{
    remove_spacer_and_prefix(str);

    std::string::size_type pos = str.find_first_of('=');
    if(pos==std::string::npos)
        return false;

    std::string name = str.substr(0, pos);
    std::string value = str.substr(pos+1);

    AddItem(name, value);

    return true;
}

ParamTreeItem *ParamTreeItem::add_node(std::string str)
{
   remove_spacer_and_prefix(str);
   return AddNode(str);
}

void ParamTreeItem::remove_spacer_and_prefix(std::string& str)
{
    while(str.compare(0, spacer.size(), spacer)==0)
        str.erase(0, spacer.size());

    if(!str.size())
        return;

    if(str.compare(0, node_prefix.size(), node_prefix)==0)
        str.erase(0, node_prefix.size());

    if(!str.size())
        return;

    if(str.compare(0, node_prefix.size(), node_prefix)==0)
        str.erase(0, node_prefix.size());

}

bool ParamTreeItem::is_node(std::string str)
{
    std::string::size_type pos=0;
    while(str.compare(pos, spacer.size(), spacer)==0)
        pos+=spacer.size();

    return (str[pos]=='$');
}

std::string ParamTreeItem::get_pair(const std::string& str)
{
  std::string s(str);
  s.insert(s.cbegin() + s.find_first_of('$'), '$');
  return s;
}


