#ifndef PARAM_TREE_H
#define PARAM_TREE_H

#include <string>
#include <deque>
#include <vector>
#include <utility>
#include <type_traits>

class ParamTreeItem{

public:

    ParamTreeItem(bool is_node, std::string name="Unknown_parameter", std::string value=""){node=is_node; this->name=name; this->value=value;}
    ~ParamTreeItem(){for(auto item : items) delete item;}

    ParamTreeItem* AddNode(std::string name){ParamTreeItem* node = new ParamTreeItem(true, name); items.push_back(node); return node;}
    void AddItem(std::string name, std::string value){items.push_back(new ParamTreeItem(false, name, value));}

    template<class Iterator>
    typename std::enable_if<std::is_same<typename Iterator::value_type, std::string>::value, bool>::type
    AddStrings(Iterator first, Iterator end){
        Iterator it = first;

        while(it!=end){

            if(!is_node(*it)){

               if(!add_item(*it))
                   return false;
               it++;

            }else{

                std::string pair = get_pair(*it);

                it++;
                Iterator node_first=it;

                bool found=false;
                while(it!=end && !found){
                    if(*it==pair){
                        found=true;
                        break;
                    }
                    it++;
                }

                if(!found)
                    return false;

                Iterator node_end=it;
                it++;

                ParamTreeItem* node = add_node(*node_end);
                if(!node)
                    return false;

                if(!node->AddStrings(node_first, node_end))
                    return false;

            }

        }

        return true;
    }


    static std::string spacer;
    static std::string node_prefix;

    bool IsNode(){return node;}

    std::string String(){return node?(node_prefix+name):(name+"="+value);}
    void GetStrings(std::vector<std::string>&, bool add_spacer=true);

private:

    bool node;
    std::string name;
    std::string value;

    std::deque<ParamTreeItem*> items;

    bool add_item(std::string);
    ParamTreeItem* add_node(std::string);

    void remove_spacer_and_prefix(std::string&);

    bool is_node(std::string);
    std::string get_pair(const std::string&);

};


#endif // PARAM_TREE_H
